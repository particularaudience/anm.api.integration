export const renderResult = (
  apiName,
  productRef,
  refIdsInCart,
  customerId,
  requestUrl,
  result,
) => {
  // dom and bind result
  let resultTable = null
  if (customerId) {
    resultTable = document.querySelector('.call-api-customerId-tbody')
    let tableHeader = document.querySelector('.call-api-customerId-header')
    tableHeader.innerText = `Call API with customerId ${customerId}`
  } else {
    resultTable = document.querySelector('.call-api-without-customerId-tbody')
  }
  let resultRow = document.createElement('tr')

  let campaignTacticCol = document.createElement('th')
  campaignTacticCol.setAttribute('scope', 'row')
  campaignTacticCol.innerText = apiName
  resultRow.appendChild(campaignTacticCol)

  let currentRefIdCol = document.createElement('td')
  currentRefIdCol.innerText = productRef
  resultRow.appendChild(currentRefIdCol)

  let refIdsCol = document.createElement('td')
  refIdsCol.innerText = JSON.stringify(refIdsInCart)
  resultRow.appendChild(refIdsCol)

  let requestUrlCol = document.createElement('td')
  requestUrlCol.innerText = requestUrl
  requestUrlCol.style.maxWidth = "400px"
  requestUrlCol.style.wordBreak = "break-all"
  resultRow.appendChild(requestUrlCol)

  let responseResultCol = document.createElement('td')
  responseResultCol.innerText = JSON.stringify(result)
  responseResultCol.style.maxWidth = "400px"
  responseResultCol.style.wordBreak = "break-all"
  resultRow.appendChild(responseResultCol)

  resultTable.appendChild(resultRow)
}

export const renderCustomerIdResult = (customerId = '') => {
  let customerIdPresentOnPage = document.querySelector('#customerId')
  customerIdPresentOnPage.innerText = customerId
}