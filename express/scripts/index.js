import { buildRequestUrl } from './build-request-url.js'
import { requestApi } from './request-api.js'
import { renderResult, renderCustomerIdResult } from './render-result.js'
import { getCustomerIdFromCookie } from './get-customerId-from-cookie.js'

const WEBSITE_ID = '11111111-1111-1111-1111-111111111111'
const BASE_API = 'https://recs-1a.particularaudience.com'

const API_NAME = {
  WITH_CUSTOMER_ID: {
    RecentlyViewed: 'RecentlyViewed',
    HistoricViewed: 'HistoricViewed',
    BoughtTogetherRecentlyViewed: 'BoughtTogetherRecentlyViewed',
    ViewedTogetherRecentlyViewed: 'ViewedTogetherRecentlyViewed'
  },
  WITHOUT_CUSTOMER_ID: {
    PopularItems: 'PopularItems',
    ViewedTogether: 'ViewedTogether',
    BoughtTogether: 'BoughtTogether',
    BoughtTogetherCartContent: 'BoughtTogetherCartContent',
    VisuallySimilar: 'VisuallySimilar',
    SimilarAttributes: 'SimilarAttributes',
  }
}
const PRODUCT_REFID = 'pr000'
const PRODUCT_REFID_VISUAL_SIMILAR = 'pr000'
const PRODUCT_REFID_CALLED_WITH_CUSTOMER_ID = ''
const PRODUCT_REFIDS_IN_CART = ['pr000']
const PRODUCT_REFIDS_IN_CART_WITH_CUSTOMER_ID = []

const getOrObserveCustomerIdThenCallApi = async () => {
  const customerIdFromCookie = getCustomerIdFromCookie()
  if (customerIdFromCookie) {
    // customerId / PAC exist in the cookie, get customerId from cookie
    await callListApiWithCustomerId(customerIdFromCookie)
  } else {
    // check if customerId / PAC is binded to body attribute at this time, get customerId from body with data=pacid attribute name
    if (document.body && document.body.hasAttribute('data-pacid')) {
      const customerIdFromBodyAttr = document.body.getAttribute('data-pacid')
      await callListApiWithCustomerId(customerIdFromBodyAttr)
    }
    // observe customerId / PAC from body attribute name data-pacid
    const config = { attributes: true }
    const bodyChangedCallback = async () => {
      console.log('observe body changed')
      if (document.body.hasAttribute('data-pacid')) {
        const customerIdFromBodyAttr = document.body.getAttribute('data-pacid')
        await callListApiWithCustomerId(customerIdFromBodyAttr)
      }
    }
    const observer = new MutationObserver(bodyChangedCallback)
    observer.observe(document.body, config)
  }
}

const requestAndRenderData = async (
  apiName,
  productRef = '',
  refIdsInCart = [],
  customerId = '',
) => {
  // return response contain list of product refId
  console.log(
    `requestData apiName: ${apiName}, productRef: ${productRef}, refIdsInCart: ${refIdsInCart}`,
  )
  const requestUrl = buildRequestUrl(BASE_API, WEBSITE_ID, apiName, productRef, refIdsInCart, customerId)
  const responseResult = await requestApi(requestUrl)
  renderResult(
    apiName,
    productRef,
    refIdsInCart,
    customerId,
    requestUrl,
    responseResult,
  )
  return responseResult
}

const callListApiWithCustomerId = async (customerId) => {
  renderCustomerIdResult(customerId)
  await requestAndRenderData(API_NAME.WITH_CUSTOMER_ID.RecentlyViewed, PRODUCT_REFID_CALLED_WITH_CUSTOMER_ID, PRODUCT_REFIDS_IN_CART_WITH_CUSTOMER_ID, customerId)
  await requestAndRenderData(API_NAME.WITH_CUSTOMER_ID.HistoricViewed, PRODUCT_REFID_CALLED_WITH_CUSTOMER_ID, PRODUCT_REFIDS_IN_CART_WITH_CUSTOMER_ID, customerId)
  await requestAndRenderData(API_NAME.WITH_CUSTOMER_ID.BoughtTogetherRecentlyViewed, PRODUCT_REFID_CALLED_WITH_CUSTOMER_ID, PRODUCT_REFIDS_IN_CART_WITH_CUSTOMER_ID, customerId)
  await requestAndRenderData(API_NAME.WITH_CUSTOMER_ID.ViewedTogetherRecentlyViewed, PRODUCT_REFID_CALLED_WITH_CUSTOMER_ID, PRODUCT_REFIDS_IN_CART_WITH_CUSTOMER_ID, customerId)
}

const callListApiWithoutCustomerId = async () => {
  await requestAndRenderData(API_NAME.WITHOUT_CUSTOMER_ID.PopularItems)
  await requestAndRenderData(API_NAME.WITHOUT_CUSTOMER_ID.ViewedTogether, PRODUCT_REFID)
  await requestAndRenderData(API_NAME.WITHOUT_CUSTOMER_ID.BoughtTogether, PRODUCT_REFID)
  await requestAndRenderData(API_NAME.WITHOUT_CUSTOMER_ID.BoughtTogetherCartContent, '', PRODUCT_REFIDS_IN_CART)
  await requestAndRenderData(API_NAME.WITHOUT_CUSTOMER_ID.VisuallySimilar, PRODUCT_REFID_VISUAL_SIMILAR)
  await requestAndRenderData(API_NAME.WITHOUT_CUSTOMER_ID.SimilarAttributes, PRODUCT_REFID)
}

window.addEventListener('DOMContentLoaded', async () => {
  await callListApiWithoutCustomerId()
  await getOrObserveCustomerIdThenCallApi()
})