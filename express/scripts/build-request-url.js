export const buildRequestUrl = (baseApi, websiteId, apiName, productRef = '', refIdsInCart = [], customerId = '') => {
  let requestUrl = ''
  let params = []
  try {
    requestUrl = `${baseApi}/1.0/ClientRecom/${websiteId}/${apiName}`

    if (productRef) {
      params.push(`productRef=${productRef}`)
    }

    if (customerId) {
      params.push(`customerId=${customerId}`)
    }

    for (let i = 0; i < refIdsInCart.length; i++) {
      params.push(`items[${i}]=${encodeURIComponent(refIdsInCart[i])}`)
    }

    if (params && params.length > 0) {
      let paramURI = params.join('&')
      requestUrl += `?${paramURI}`
    }
    console.log(`RequestUrl: ${requestUrl}`)
  } catch (error) {
    console.error('At buildRequestUrl' + error)
  }
  return requestUrl
}