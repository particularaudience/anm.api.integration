export const requestApi = async (requestUrl, option = {}) => {
  let response = null
  let data = null
  try {
    try {
      response = await fetch(requestUrl, option)
    } catch (error) {
      console.log(`Can not get response of requestUrl  ${requestUrl}`)
    }
    if (response) {
      if (response.status === 200) {
        data = await response.json()
      } else {
        throw new Error('Response status is not equal 200')
      }
    }
  } catch (error) {
    console.error(
      `Can not _getData from ${requestUrl}, response status: ${
        response && response.status
      }, response message: ${response && response.statusText}`,
    )
    data = null
  }

  return data
}