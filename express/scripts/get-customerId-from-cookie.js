export const getCookie = (cookieName) => {
  let name = cookieName + '='
  let cookieAttributes = document.cookie.split(';')
  for (var i = 0; i < cookieAttributes.length; i++) {
    let cookieAttribute = cookieAttributes[i]
    while (cookieAttribute.charAt(0) == ' ') {
      cookieAttribute = cookieAttribute.substring(1)
    }
    if (cookieAttribute.indexOf(name) == 0) {
      return cookieAttribute.substring(name.length, cookieAttribute.length)
    }
  }
  return ''
}

export const getCustomerIdFromCookie = () => {
  return getCookie('PAC')
}