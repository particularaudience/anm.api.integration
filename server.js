const { port } = require('./config')
const http = require('http')
const express = require('express')
const path = require('path')
const app = express()
app.use(express.json())
app.use(express.static('express'))

const router = express.Router()

router.get('/', (req, res) => {
  res.sendFile(path.join(__dirname + '/express/index.html'))
})

router.get('/gtm', (req, res) => {
  res.sendFile(path.join(__dirname + '/express/gtm.html'))
})

app.use('/', router)

const server = http.createServer(app)

server.listen(port)
console.debug(`Server listening on port ${port}`)
