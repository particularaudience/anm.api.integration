# Particular Audience Integration Sample
This is a project to demonstrate how to use the Particular Audience Recommendation API.
Particular Audience is a platform that enhances e-commerce stores with recommendations, personalisation, merchandise customisation, search and visual search. You can find more information at [particularaudience.com](https://particularaudience.com/).

This sample project demonstrates how to integrate the Recommendation API. The project can be downloaded and run to call the Particular Audience Recommendation APIs.

The project includes a mock ID key that returns mock data, allowing you to test your integration immediately.

Once you are comfortable with the code, please request a sandbox key from Particular Audience to perform functional testing. You can message your account manager directly or if unsure who your account manager is, please email [support@particularaudience.com](mailto:support@particularaudience.com) to get your sandbox key.

## How to run this project?

* Step 1: Install [Nodejs](https://nodejs.org/en/) if you don't have it yet
* Step 2: Type `npm install` to install packages
* Step 3: Type `npm start` to run the project
* Step 4: Launch `localhost:8080` on browser


## Configuration in this project
This is a sample of the script tag that needs to be injected on every html page, in the head of the html. When you are ready to receive production API details we will provide you with an updated script tag.
```
<script>window.addPAC = function (c, e) { document.cookie = 'PAC=' + c + ';' + e + ';' }</script>
<script src="https://cdn.particularaudience.com/js/sandbox/t.js"></script>
```
The sandbox Recommendation API domain is `https://int-api.particularaudience.com` 


### Where do I get the customerID?
You can get the customerID from the cookie named `PAC`. Please observe the `data-pacid` attribute in the body tag as the customerID will be written here.


#### Script to get the customerID from the cookie named `PAC`
> This script is an example of how you can acquire the customerID from the stored cookie.
```
const getCookie = (cookieName) => {
  let name = cookieName + '='
  let cookieAttributes = document.cookie.split(';')
  for (var i = 0; i < cookieAttributes.length; i++) {
    let cookieAttribute = cookieAttributes[i]
    while (cookieAttribute.charAt(0) == ' ') {
      cookieAttribute = cookieAttribute.substring(1)
    }
    if (cookieAttribute.indexOf(name) == 0) {
      return cookieAttribute.substring(name.length, cookieAttribute.length)
    }
  }
  return ''
}
const customerId = getCookie('PAC')
```

#### Script to observe customerID from body attribute named `data-pacid`
> This script is an example of how you can acquire the customerID from the body tag attribute.
```
const observeConfig = { attributes: true }
const bodyChangedCallback = async () => {
  if (document.body.hasAttribute('data-pacid')) {
    const customerId = document.body.getAttribute('data-pacid')
  }
}
const observer = new MutationObserver(bodyChangedCallback)
observer.observe(document.body, observeConfig)
```
