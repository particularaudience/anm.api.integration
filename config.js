const dotenv = require('dotenv');
dotenv.config();
module.exports = {
  env: process.env.ENV,
  port: process.env.PORT
};